import 'package:flutter/material.dart';

const backgroundColor = Color(0xFFFFFFFF);
const txtColor = Color(0xFF333333);
const txtLightColor = Color(0xffacacac);
const dfPadding = 20.0;

// Running poster stuffs
const posterRunning = [
  'assets/images/poster/running/run4.jpg',
  'assets/images/poster/running/run3.jpg',
  'assets/images/poster/running/run5.png'
];
const titleRunning = [
  'ULTRABOOST 21 SHOES',
  'RUNNING CAMP',
  'ULTRABOOST X SHOES'
];

// Originals poster stuffs
const posterOg = [
  'assets/images/poster/original/og1.jpg',
  'assets/images/poster/original/og2.jpg',
  'assets/images/poster/original/og3.webp'
];
const titleOg = [
  "STOCKHOLM OASIS SHOES",
  "GO SKATE DAYS",
  "OG DRESSERS"
];

// Football poster stuffs
const posterFootball = [
  'assets/images/poster/football/football2.webp',
  'assets/images/poster/football/football3.jpg',
  'assets/images/poster/football/football1.jpg'
];
const titleFootball = [
  'FIFA WORLDCUP MATCH BALL',
  'THOMAS MULLER',
  'STAND WITH M.U.'
];

// Basketball poster stuffs
const posterBasketball = [
  'assets/images/poster/basketball/basket1.jpg',
  'assets/images/poster/basketball/basket2.jpg',
  'assets/images/poster/basketball/basket3.jpg'
];
const titleBasketball = [
  'DERICK ROSE SHOES',
  'BASKETBALL WITH US',
  'JAYLEN BROWN'
];

// Golf poster stuffs
const posterGolf = [
  'assets/images/poster/golf/golf1.png',
  'assets/images/poster/golf/golf2.jpg',
  'assets/images/poster/golf/golf3.jpg',
];
const titleGolf = [
  'TOUR 360 XT SHOES',
  'AARON WISE',
  'JOIN US'
];