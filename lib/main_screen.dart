import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:midterm_519h0077/resources/constants.dart';
import 'package:midterm_519h0077/views/cart/cart_screen.dart';
import 'package:midterm_519h0077/views/radar_screen/radar_screen.dart';
import 'package:midterm_519h0077/views/wishlist/wishlist_screen.dart';
import 'package:midterm_519h0077/views/home/home_screen.dart';
import 'package:midterm_519h0077/views/shop/shop_screen.dart';


class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _page = 0;
  late PageController pageController;

  @override
  void initState() {
    super.initState();
    pageController = PageController();
  }

  @override
  void dispose() {
    super.dispose();
    pageController.dispose();
  }

  void navigationTapped(int page) {
    pageController.jumpToPage(page);
  }

  void onPageChanged(int page) {
    setState(() {
      _page = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        color: backgroundColor,
        padding: const EdgeInsets.only(top: 10.0),
        child: CupertinoTabBar(
          border: null,
          onTap: navigationTapped,
          backgroundColor: backgroundColor,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.local_fire_department_outlined, color: _page == 0 ? txtColor : txtLightColor,),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.manage_search, color: _page == 1 ? txtColor : txtLightColor,),
              label: ''
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite_border_outlined, color: _page == 2 ? txtColor : txtLightColor,),
              label: ''
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.shopping_bag_outlined, color: _page == 3 ? txtColor : txtLightColor,),
              label: ''
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.track_changes, color: _page == 4 ? txtColor : txtLightColor,),
              label: ''
            ),
          ],
        ),
      ),
      
      body: SafeArea(
        child: PageView(
          children: const [
            HomeScreen(),
            ShopScreen(),
            WishlistScreen(),
            CartScreen(),
            RadarScreen()
          ],
          physics: const NeverScrollableScrollPhysics(),
          controller: pageController,
          onPageChanged: onPageChanged,
        ),
      ),
    );
  }
}


