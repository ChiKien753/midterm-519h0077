import 'package:flutter/material.dart';
import 'package:midterm_519h0077/resources/constants.dart';
import 'package:midterm_519h0077/views/authentication/authentication_screen.dart';
import 'package:midterm_519h0077/views/home/components/categories.dart';
import 'package:midterm_519h0077/views/home/components/item_card.dart';
import 'package:midterm_519h0077/views/search_screen.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({ Key? key }) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DROPS", style: TextStyle(color: txtColor, fontWeight: FontWeight.bold),),
        backgroundColor: backgroundColor,
        elevation: 1,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: ((context) => const SearchScreen()),
                ),
              );
            },
            icon: const Icon(
              Icons.search,
              color: txtColor,
            ),
          ),
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: ((context) => const AuthenticationScreen()),
                ),
              );
            },
            icon: const Icon(
              Icons.person_outline,
              color: txtColor,
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          const Categories(),
          Expanded(
            child: ListView.builder(
              itemCount: posterRunning.length,
              itemBuilder: (BuildContext context, int index) {
                return loadProduct(context, index);
              },
            ),
          ),
        ],
      ),
    );
  }

  ItemCard loadProduct(BuildContext context, int index){
    if (selectedCategory == 0) {
      return ItemCard(img: posterRunning[index], title: titleRunning[index]); 
    }
    
    if (selectedCategory == 1) {
      return ItemCard(img: posterOg[index], title: titleOg[index]); 
    }

    if (selectedCategory == 2) {
      return ItemCard(img: posterFootball[index], title: titleFootball[index]); 
    }

    if (selectedCategory == 3) {
      return ItemCard(img: posterBasketball[index], title: titleBasketball[index]); 
    }
    return ItemCard(img: posterGolf[index], title: titleGolf[index]); 
  }
}