import 'package:flutter/material.dart';
import 'package:midterm_519h0077/main_screen.dart';
import 'package:midterm_519h0077/resources/constants.dart';

int selectedCategory = 0;

class Categories extends StatefulWidget {
  const Categories({ Key? key }) : super(key: key);

  @override
  State<Categories> createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  List<String> categories = ["RUNNING", "ORIGINALS", 'FOOTBALL', "BASKETBALL", 'GOLF'];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 14),
      child: SizedBox(
        height: 20,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: categories.length,
          itemBuilder: (context, index) => buildCategory(index),
        ),
      ),
    );
  }

Widget buildCategory(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          Navigator.pushReplacement(
            context,
            PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => const MainScreen(),
                transitionDuration: Duration.zero,
                reverseTransitionDuration: Duration.zero),
          );
          selectedCategory = index;
        });
      },
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: dfPadding / 2),
        padding: const EdgeInsets.symmetric(horizontal: dfPadding / 2),
        decoration: BoxDecoration(
          border: Border.all(
            color: selectedCategory == index ? txtColor : txtLightColor,
            width: 1
          ),
          
        ),
        child: Text(
            categories[index],
            style: TextStyle(color: selectedCategory == index ? txtColor : txtLightColor),
        ),
      ),
    );
  }
}