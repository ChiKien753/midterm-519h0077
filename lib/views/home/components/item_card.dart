import 'package:flutter/material.dart';
import 'package:midterm_519h0077/resources/constants.dart';
import 'package:midterm_519h0077/views/all_item_screen/all_item_screen.dart';

class ItemCard extends StatefulWidget {
  String img;
  String title;
  ItemCard({Key? key, required this.img, required this.title}) : super(key: key);

  @override
  State<ItemCard> createState() => _ItemCardState();
}

class _ItemCardState extends State<ItemCard> {
  @override
  Widget build(BuildContext context) {
    return singleCard(widget.img, widget.title);
  }

  Stack singleCard(String img, String title) {
    return Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Container(
              child: Image.asset(img),
            ),
            Positioned(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(10),
                        margin: const EdgeInsets.only(bottom: 2),
                        decoration: const BoxDecoration(
                          color: backgroundColor
                        ),
                        child: Text(
                          title,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(10),
                        margin: const EdgeInsets.only(bottom: 8),
                        decoration: const BoxDecoration(
                          color: backgroundColor
                        ),
                        child: const Text(
                          "JUST DROPPED",
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontSize: 14
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: ((context) => const AllItemScreen()),
                              ),
                            );
                        },
                        child: Container(
                          width: double.infinity,
                          padding: const EdgeInsets.only(top: 13, left: 20, right: 20, bottom: 13),
                          margin: const EdgeInsets.only(bottom: 12, left: 35, right: 35),
                          decoration: BoxDecoration(
                            color: backgroundColor,
                            border: Border.all(width: 1, color: txtColor),
                          ),
                          child: Row(
                            children: const [
                              Text(
                                'SHOP NOW',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16
                                ),
                              ),
                              Expanded(child: SizedBox(), flex: 1),
                              Icon(Icons.arrow_forward)
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            
          ],
        );
  }
}