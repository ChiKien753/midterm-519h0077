import 'package:flutter/material.dart';
import 'package:midterm_519h0077/models/Product.dart';
import 'package:midterm_519h0077/resources/constants.dart';
import 'package:midterm_519h0077/views/authentication/authentication_screen.dart';
import 'package:midterm_519h0077/views/wishlist/wishlist_no_item.dart';

class WishlistScreen extends StatefulWidget {
  const WishlistScreen({ Key? key }) : super(key: key);

  @override
  State<WishlistScreen> createState() => _WishlistScreenState();
}

class _WishlistScreenState extends State<WishlistScreen> {
  bool hasLike = false;
  List<Product> listTmpProd = [];

  @override
  Widget build(BuildContext context) {
    
    for (int i = 0; i < products.length; i++){
      if (products[i].isLiked == true) {
        hasLike = true;
        listTmpProd.add(products[i]);
      }
    }
    
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'WISHLIST',
          style: TextStyle(fontWeight: FontWeight.bold, color: txtColor),
        ),
        elevation: 1,
        backgroundColor: backgroundColor,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: ((context) => const AuthenticationScreen()),
                ),
              );
            },
            icon: const Icon(
              Icons.person_outline,
              color: txtColor,
            ),
          ),
        ],
      ),
      body: hasLike ? hasItem() : const WishlistNoItem(),
      
    );
  }

  GridView hasItem() {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 5,
        crossAxisSpacing: 5,
        mainAxisExtent: 280
      ),
      itemCount: listTmpProd.length,
      itemBuilder: (BuildContext context, int index) {
        return Stack(
          children: [
            Container(
              color: const Color(0xFFECEFF0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(listTmpProd[index].image),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: Text("đ "+ listTmpProd[index].price),
                          color: backgroundColor,
                          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4)
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 6),
                          child: Text(
                            listTmpProd[index].name,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16
                            ),
                          ),
                        ),
                        Text(
                          listTmpProd[index].category,
                          style: const TextStyle(
                              fontSize: 13,
                              color: Color.fromARGB(255, 126, 126, 126)
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              right: 0,
              child: IconButton(
                icon: listTmpProd[index].isLiked == false ? const Icon(Icons.favorite_border_outlined) : const Icon(Icons.favorite, color: Colors.red,),
                onPressed: () {
                  listTmpProd[index].isLiked = !listTmpProd[index].isLiked;
                  setState(() {
                    
                  });
                },
              ),
            ),
          ],
        );
      },
    );
  }
}

