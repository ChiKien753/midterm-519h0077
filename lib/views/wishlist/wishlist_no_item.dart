import 'package:flutter/material.dart';
import 'package:midterm_519h0077/resources/constants.dart';
import 'package:midterm_519h0077/views/all_item_screen/all_item_screen.dart';

class WishlistNoItem extends StatelessWidget {
  const WishlistNoItem({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("NOTHING SAVED YET", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),),
            const SizedBox(height: 5,),
            const Text('Tap the heart icon to save items here for later', style: TextStyle(fontSize: 14),),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const AllItemScreen()),
                  ),
                );
              },
              child: Container(
                width: 300,
                margin: const EdgeInsets.only(top: 10),
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                color: txtColor,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: const [
                    Text(
                      "START SHOPPING",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 17
                      ),
                    ),
                    Expanded(child: SizedBox(), flex: 1),
                    Icon(Icons.arrow_forward, color: Colors.white,)
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}