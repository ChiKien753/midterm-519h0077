import 'package:flutter/material.dart';
import 'package:midterm_519h0077/models/Product.dart';
import 'package:midterm_519h0077/resources/constants.dart';
import 'package:midterm_519h0077/views/item_detail/item_detail_screen.dart';
import 'package:midterm_519h0077/views/search_screen.dart';

class AllItemScreen extends StatefulWidget {
  const AllItemScreen({ Key? key }) : super(key: key);

  @override
  State<AllItemScreen> createState() => _AllItemScreenState();
}

class _AllItemScreenState extends State<AllItemScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios_new,
            color: txtColor,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: backgroundColor,
        elevation: 1,
        title: const Text(
          "All Items",
          style: TextStyle(
            color: txtColor,
          ),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: ((context) => const SearchScreen()),
                ),
              );
            },
            icon: const Icon(
              Icons.search,
              color: txtColor,
            ),
          ),
        ],
      ),
      body: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 5,
          crossAxisSpacing: 5,
          mainAxisExtent: 280
        ),
        itemCount: products.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: ((context) => ItemDetailScreen(product: products[index],)),
                ),
              );
            },
            child: Stack(
              children: [
                Container(
                  color: const Color(0xFFECEFF0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Hero(
                        tag: "${products[index].id}",
                        child: Image.asset(products[index].image),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text("đ "+ products[index].price),
                              color: backgroundColor,
                              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4)
                            ),
                            Container(
                              margin: const EdgeInsets.symmetric(vertical: 6),
                              child: Text(
                                products[index].name,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16
                                ),
                              ),
                            ),
                            Text(
                              products[index].category,
                              style: const TextStyle(
                                  fontSize: 13,
                                  color: Color.fromARGB(255, 126, 126, 126)
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  right: 0,
                  child: IconButton(
                    icon: products[index].isLiked == false ? const Icon(Icons.favorite_border_outlined) : const Icon(Icons.favorite, color: Colors.red,),
                    onPressed: () {
                      setState(() {
                        products[index].isLiked = !products[index].isLiked;
                      });
                    },
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}