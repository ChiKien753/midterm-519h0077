import 'package:flutter/material.dart';
import 'package:midterm_519h0077/models/Store.dart';

import '../../resources/constants.dart';
import '../authentication/authentication_screen.dart';

class RadarScreen extends StatefulWidget {
  const RadarScreen({ Key? key }) : super(key: key);

  @override
  State<RadarScreen> createState() => _RadarScreenState();
}

class _RadarScreenState extends State<RadarScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ON THE RADAR", style: TextStyle(color: txtColor, fontWeight: FontWeight.bold),),
        backgroundColor: backgroundColor,
        elevation: 1,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: ((context) => const AuthenticationScreen()),
                ),
              );
            },
            icon: const Icon(
              Icons.person_outline,
              color: txtColor,
            ),
          ),
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Image.asset("assets/images/poster/radar.jpg"),
              Positioned(
                top: 230,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10),
                      margin: const EdgeInsets.only(bottom: 2),
                      decoration: const BoxDecoration(
                        color: backgroundColor
                      ),
                      child: const Text(
                        "RUN FOR THE OCEANS",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic,
                          fontSize: 19
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(10),
                      margin: const EdgeInsets.only(bottom: 8),
                      decoration: const BoxDecoration(
                        color: backgroundColor
                      ),
                      child: const Text(
                        "JOIN OUR MOVEMENT TODAY",
                        style: TextStyle(
                          fontStyle: FontStyle.italic,
                          fontSize: 13
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        
                      },
                      child: Container(
                        width: double.infinity,
                        padding: const EdgeInsets.only(top: 13, left: 20, right: 20, bottom: 13),
                        margin: const EdgeInsets.only(bottom: 12, left: 35, right: 35),
                        decoration: BoxDecoration(
                          color: backgroundColor,
                          border: Border.all(width: 1, color: txtColor),
                        ),
                        child: Row(
                          children: const [
                            Text(
                              'LEARN MORE',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16
                              ),
                            ),
                            Expanded(child: SizedBox(), flex: 1),
                            Icon(Icons.arrow_forward)
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const Padding(
            padding: EdgeInsets.only(left: 20, top: 20, bottom: 10),
            child: Text(
              "STORES NEARBY",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 19,
              ),
            ),
          ),
          SizedBox(
            height: 152,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: stores.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  width: 260,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            const Icon(Icons.store_mall_directory_outlined),
                            const SizedBox(width: 2,),
                            Text(stores[index].name, style:const TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        const SizedBox(height: 20,),
                        Padding(
                          padding: const EdgeInsets.only(left: 24),
                          child: Text(stores[index].street),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 24),
                          child: Text(stores[index].city),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 24),
                          child: Text(stores[index].country),
                        ),
                        const SizedBox(height: 15,),
                        Padding(
                          padding: const EdgeInsets.only(left: 24),
                          child: Row(
                            children: [
                              const Icon(Icons.location_on_outlined),
                              Text(stores[index].km.toString() + " km")
                            ],
                          ),
                        )
                      ],
                    )
                );
              },
            ),
          )
        ],
      ),
    );
  }
}