import 'package:flutter/material.dart';
import 'package:midterm_519h0077/models/Product.dart';
import 'package:midterm_519h0077/resources/constants.dart';

class ItemDetailScreen extends StatefulWidget {
  Product product;
  ItemDetailScreen({ Key? key, required this.product }) : super(key: key);

  @override
  State<ItemDetailScreen> createState() => _ItemDetailScreenState();
}

class _ItemDetailScreenState extends State<ItemDetailScreen> {
  bool isLiked = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFECEFF0),
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.close, color: txtColor,),
          ),
        ],
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Hero(
              tag: "${widget.product.id}",
              child: Image.asset(widget.product.image),
            ),
            Stack(
              children: [
                Container(
                  height: 50,
                  color: const Color(0xFFECEFF0),
                ),
                Positioned(
                  left: 26,
                  bottom: 23,
                  child: Container(
                    padding: const EdgeInsets.all(4),
                    color: Colors.white,
                    child: const Text(
                      "NEW",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.product.name,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          Text("đ " + widget.product.price,
                            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                          ),
                          const SizedBox(width: 20,),
                          Text(
                            widget.product.category,
                            style: const TextStyle(fontSize: 17, color: Color.fromARGB(255, 126, 126, 126)),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                const Spacer(),
                IconButton(
                  onPressed: () {
                    widget.product.isLiked = !widget.product.isLiked;
                    setState(() {
                      
                    });
                  },
                  icon: widget.product.isLiked ? const Icon(Icons.favorite, color: Colors.red,) : const Icon(Icons.favorite_outline),
                ),
                const SizedBox(width: 15,)
              ],
            ),
            GestureDetector(
              onTap: () {
                widget.product.isCart = !widget.product.isCart;
                Navigator.pop(context);
              },
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 30),
                padding: const EdgeInsets.all(10),
                color: txtColor,
                child: Row(
                  children: const [
                    Text(
                      "ADD TO CART",
                      style: TextStyle(
                        color: backgroundColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                    Spacer(),
                    Icon(Icons.arrow_forward, color: backgroundColor,),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}