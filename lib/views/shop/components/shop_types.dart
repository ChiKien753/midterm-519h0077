import 'package:flutter/material.dart';
import 'package:midterm_519h0077/resources/constants.dart';

class ShopTypes extends StatefulWidget {
  String type;
  ShopTypes({ Key? key, required this.type}) : super(key: key);

  @override
  State<ShopTypes> createState() => _ShopTypesState();
}

class _ShopTypesState extends State<ShopTypes> {

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(widget.type, 
      style: const TextStyle(
        color: txtColor,
        fontWeight: FontWeight.bold,
        ),
      ), 
      trailing: const Icon(Icons.navigate_next),
      onTap: () {},
    );
  }
}