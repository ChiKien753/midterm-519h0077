import 'package:flutter/material.dart';
import 'package:midterm_519h0077/models/Product.dart';
import 'package:midterm_519h0077/resources/constants.dart';
import 'package:midterm_519h0077/views/all_item_screen/all_item_screen.dart';
import 'package:midterm_519h0077/views/item_detail/item_detail_screen.dart';

class ShopList extends StatefulWidget {
  int index;
  List<String> list;
  ShopList({ Key? key, required this.index, required this.list }) : super(key: key);

  @override
  State<ShopList> createState() => _ShopListState();
}

class _ShopListState extends State<ShopList> {
  @override
  Widget build(BuildContext context) {
    int prodLen; 
    if (widget.index == 0) {
      prodLen = newArrivals.length;
    }
    else if (widget.index == 1) {      
      prodLen = basketball.length;
    }
    else if (widget.index == 2) {
      prodLen = slides.length;
    }
    else {
      prodLen = outlet.length;
    }
    return Container(
      margin: const EdgeInsets.only(top: 25),
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                widget.list[widget.index],
                style:
                  const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                ),
              ),
              const Expanded(child: SizedBox(), flex: 1),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const AllItemScreen()),
                    ),
                  );
                },
                child: Column(
                  children: [
                    const Text('SEE ALL',style: TextStyle(fontWeight: FontWeight.bold),),
                    Container(
                      margin: const EdgeInsets.only(top: 2),
                      width: 50,
                      height: 2,
                      color: txtColor,
                    ),
                  ],
                ),
              )
            ],
          ),
          SizedBox(
            height: 230,
            child: ListView.builder(
              itemCount: prodLen,
              scrollDirection: Axis.horizontal,
              itemBuilder: buildItemList,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildItemList(BuildContext context, int index) {
    Product tmpProd;
    if (widget.index == 0) {
      tmpProd = newArrivals[index];
    }
    else if (widget.index == 1) {
      tmpProd = basketball[index];
    }
    else if (widget.index == 2) {
      tmpProd = slides[index];
    }
    else {
      tmpProd = outlet[index];
    }
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: ((context) => ItemDetailScreen(product: tmpProd,)),
          ),
        );
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 8, right: 8, top: 12,),
        child: Stack(
          children: [
            Column(
              children: [
                SizedBox(
                  height: 150,
                  width: 150,
                  child: Image.asset(tmpProd.image),
                ),
                Container(
                  color: const Color.fromARGB(255, 226, 226, 226),
                  width: 150,
                  child: Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: '\n  ' + (tmpProd.name),
                          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                        TextSpan(
                          text: '\n   ' + (tmpProd.category) + '\n',
                          style: const TextStyle(fontSize: 12, color: Color.fromARGB(255, 150, 150, 150))
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              top: 135,
              left: 8,
              child: Container(
                child: Text('đ '+tmpProd.price),
                color: backgroundColor,
                padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
              ),
            ),
          ],
        ),
      ),
    );
  }
}