import 'package:flutter/material.dart';
import 'package:midterm_519h0077/resources/constants.dart';

class ShopCategories extends StatefulWidget {
  const ShopCategories({ Key? key }) : super(key: key);

  @override
  State<ShopCategories> createState() => _ShopCategoriesState();
}

class _ShopCategoriesState extends State<ShopCategories> {
  List<String> categories = ["MEN", "WOMEN", 'KIDS'];
  int isSelected = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        border: Border(
          top: BorderSide(width: 1, color: txtLightColor),
        ),
      ),
      child: SizedBox(
        height: 45,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: categories.length,
          itemBuilder: (context, index) => buildCategory(index),
        ),
      ),
    );
  }

  Widget buildCategory(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isSelected = index;
        });
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: dfPadding),
        child: Column(
          children: [
            Expanded(
              child: Center(
                child: Text(
                    categories[index],
                    textAlign: TextAlign.center,
                    style: TextStyle(color: txtColor, fontWeight: isSelected == index ? FontWeight.bold : FontWeight.normal),
                ),
              ),
            ),
            Container(
              height: 2,
              width: 30,
              color: isSelected == index ? txtColor : Colors.transparent,
            ),
          ],
        ),
      ),
    );
  }
}