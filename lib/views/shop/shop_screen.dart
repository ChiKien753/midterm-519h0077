import 'package:flutter/material.dart';
import 'package:midterm_519h0077/resources/constants.dart';
import 'package:midterm_519h0077/views/authentication/authentication_screen.dart';
import 'package:midterm_519h0077/views/search_screen.dart';
import 'package:midterm_519h0077/views/shop/components/shop_list.dart';
import 'package:midterm_519h0077/views/shop/components/shop_types.dart';

import 'components/shop_categories.dart';


class ShopScreen extends StatefulWidget {
  const ShopScreen({ Key? key }) : super(key: key);

  @override
  State<ShopScreen> createState() => _ShopScreenState();
}

class _ShopScreenState extends State<ShopScreen> {
  List<String> list = ['NEW ARRIVALS', 'BASKETBALL', 'SLIDES', 'OUTLET'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "SHOP",
          style: TextStyle(
            color: txtColor,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: backgroundColor,
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: ((context) => const SearchScreen()),
                ),
              );
            },
            icon: const Icon(
              Icons.search,
              color: txtColor,
            ),
          ),
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: ((context) => const AuthenticationScreen()),
                ),
              );
            },
            icon: const Icon(
              Icons.person_outline,
              color: txtColor,
            ),
          ),
        ],
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              child: Column(
                children: [
                  const ShopCategories(),
                  ShopTypes(type: "SHOES"),
                  Container(
                    height: 1,
                    color: const Color.fromARGB(255, 226, 226, 226),
                  ),
                  ShopTypes(type: "CLOTHING"),
                  Container(
                    height: 1,
                    color: const Color.fromARGB(255, 226, 226, 226),
                  ),
                  ShopTypes(type: "ACCESSORIES"),
                  Container(
                    height: 1,
                    color: const Color.fromARGB(255, 226, 226, 226),
                  ),
                  ShopTypes(type: "SHOP BY SPORT"),
                  Container(
                    height: 1,
                    color: const Color.fromARGB(255, 226, 226, 226),
                  ),
                  ShopTypes(type: "SHOP BY BRAND"),
                  Container(
                    height: 1,
                    color: const Color.fromARGB(255, 226, 226, 226),
                  ),
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return ShopList(index: index, list: list);
              }, 
              childCount: list.length,
            ),
          ),
        ],
      ),
    );
  }
}