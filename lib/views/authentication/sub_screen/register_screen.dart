import 'package:flutter/material.dart';
import 'package:midterm_519h0077/resources/constants.dart';

import 'components/authentication_method.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({ Key? key }) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("REGISTER", style: TextStyle(color: txtColor, fontSize: 24, fontWeight: FontWeight.bold),),
        backgroundColor: backgroundColor,
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.close,
              color: txtColor,
            ),
          ),
        ],
      ),
      body: const AuthenticationMethod(),
      bottomSheet: Container(
        color: backgroundColor,
        height: 200,
        margin: const EdgeInsets.symmetric(horizontal: 20),
        child: const Text.rich(
          TextSpan(
            text: "By clicking REGISTER, I \n- agree that I have read, understood and acepted the ",
            children: [
              TextSpan(
                text: 'Privacy Note',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.underline
                ),
              ),
              TextSpan(
                text: ' and ',
              ),
              TextSpan(
                text: 'Terms and Conditions',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.underline
                ),
              ),
              TextSpan(
                text: '\n- hereby consent to ther use of my personal data for marketing and promotional purposes as set out in the adidas ',
              ),
              TextSpan(
                text: 'Privacy Notice',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.underline
                ),
              ),
              TextSpan(
                text: '\n- hereby consent to the transfer, sharing, use, collection and disclosure of my personal data to third parties as set out in the adidas ',
              ),
              TextSpan(
                text: 'Privacy Notice',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.underline
                ),
              ),
              TextSpan(
                text: '\n If you do not agree to any of the above, please do not click REGISTER and exit the App',
              ),
            ],
          ),
        ),
        // child: RichText(
        //   text: const TextSpan(
        //     text: "By clicking REGISTER, I ",
        //     style: TextStyle(color: txtColor),
        //     children: [
        //       TextSpan(
        //         text: "Terms and Conditions",
        //           style: TextStyle(
        //             color: txtColor,
        //             fontWeight: FontWeight.bold,
        //             decoration: TextDecoration.underline,
        //           )
        //       ),
        //     ],
        //   ),
        // ),
      ),
    );
  }
}