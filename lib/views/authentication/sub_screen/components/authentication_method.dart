import 'package:flutter/material.dart';
import 'package:midterm_519h0077/resources/constants.dart';

class AuthenticationMethod extends StatelessWidget {
  const AuthenticationMethod({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              const SizedBox(height: 30,),
              Container(
                height: 40,
                width: MediaQuery.of(context).size.width - 40,
                color: txtColor,
                child: Center(
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: const TextSpan(
                      children: [
                        WidgetSpan(child: Icon(Icons.apple, color: backgroundColor,)),
                        TextSpan(
                          text: ' Sign in with apple',
                          style: TextStyle(
                            color: backgroundColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 20
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 20,),
              Row(
                children: [
                  Container(color: const Color.fromARGB(255, 194, 194, 194), width: 150, height: 1,),
                  const Text('    OR    '),
                  Container(color: const Color.fromARGB(255, 194, 194, 194), width: 150, height: 1,),
                ],
              ),
              const SizedBox(height: 16,),
              Container(
                child: RichText(
                    text: const TextSpan(
                      children: [
                        WidgetSpan(child: Icon(Icons.facebook, color: Color(0xFF0572e6), size: 26,)),
                        TextSpan(
                          text: ' Facebook',
                          style: TextStyle(
                            color: txtColor,
                            fontSize: 16
                          ),
                        ),
                      ],
                    ),
                  ),
              ),
              const SizedBox(height: 16,),
              Container(
                width: 350,
                height: 1,
                color: const Color.fromARGB(255, 194, 194, 194),
              ),
              const SizedBox(height: 16,),
              Container(
                child: RichText(
                    text: const TextSpan(
                      children: [
                        WidgetSpan(child: Icon(Icons.email_outlined, color: txtColor, size: 26,)),
                        TextSpan(
                          text: ' Email',
                          style: TextStyle(
                            color: txtColor,
                            fontSize: 16
                          ),
                        ),
                      ],
                    ),
                  ),
              ),
              const SizedBox(height: 16,),
              Container(
                width: 350,
                height: 1,
                color: const Color.fromARGB(255, 194, 194, 194),
              ),
            ],
          ),
        ],
      ),
    );
  }
}