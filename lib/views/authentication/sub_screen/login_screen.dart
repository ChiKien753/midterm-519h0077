import 'package:flutter/material.dart';
import 'package:midterm_519h0077/resources/constants.dart';

import 'components/authentication_method.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({ Key? key }) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("LOGIN", style: TextStyle(color: txtColor, fontSize: 24, fontWeight: FontWeight.bold),),
        backgroundColor: backgroundColor,
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.close,
              color: txtColor,
            ),
          ),
        ],
      ),
      body: const AuthenticationMethod(),
      bottomSheet: Container(
        color: backgroundColor,
        height: 50,
        margin: const EdgeInsets.symmetric(horizontal: 20),
        child: RichText(
          text: const TextSpan(
            children: [
              TextSpan(
                text: "By signing up you agree to our ",
                style: TextStyle(color: txtColor)
              ),
              TextSpan(
                text: "Terms and Conditions",
                  style: TextStyle(
                    color: txtColor,
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.underline,
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}