import 'package:flutter/material.dart';
import 'package:midterm_519h0077/resources/constants.dart';
import 'package:midterm_519h0077/views/authentication/sub_screen/login_screen.dart';
import 'package:midterm_519h0077/views/authentication/sub_screen/register_screen.dart';

class AuthenticationScreen extends StatefulWidget {
  const AuthenticationScreen({ Key? key }) : super(key: key);

  @override
  State<AuthenticationScreen> createState() => _AuthenticationScreenState();
}

class _AuthenticationScreenState extends State<AuthenticationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.transparent,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(
              Icons.close,
              color: backgroundColor,
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: Image.asset(
              'assets/images/black-white.jpg',
              fit: BoxFit.fill,
              alignment: Alignment.center,
              height: double.infinity,
              width: double.infinity,
            ),
          ),
          const SizedBox(
            height: 110
          ),
        ],
      ),
      bottomSheet: SizedBox(
        height: 110,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: ((context) => const LoginScreen()),
                      ),
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: txtColor,
                        width: 0.75
                      ),
                    ),
                    width: MediaQuery.of(context).size.width - 40,
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: const Text("LOGIN", style: TextStyle(fontWeight: FontWeight.bold),),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: ((context) => const RegisterScreen()),
                      ),
                    );
                  },
                  child: Container(
                    color: txtColor,
                    width: MediaQuery.of(context).size.width - 40,
                    margin: const EdgeInsets.only(top: 10),
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: const Text(
                      "REGISTER",
                      style: TextStyle(fontWeight: FontWeight.bold, color: backgroundColor),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}