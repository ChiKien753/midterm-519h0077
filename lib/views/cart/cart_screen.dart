import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:midterm_519h0077/resources/constants.dart';
import 'package:midterm_519h0077/views/authentication/authentication_screen.dart';

import '../../models/Product.dart';
import 'cart_no_item.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({ Key? key }) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  bool hasItem = false;
  List<Product> listTmpProd = [];
  int total = 0;
  var priceArr = [];
  var price;

  @override
  Widget build(BuildContext context) {
    for (int i = 0; i < products.length; i++){
      if (products[i].isCart == true) {
        hasItem = true;
        listTmpProd.add(products[i]);
        priceArr = products[i].price.split('.');
        price = priceArr.join();
        total += int.parse(price);
      }
    } 

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'SHOPPING BAG',
          style: TextStyle(fontWeight: FontWeight.bold, color: txtColor),
        ),
        elevation: 1,
        backgroundColor: backgroundColor,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: ((context) => const AuthenticationScreen()),
                ),
              );
            },
            icon: const Icon(
              Icons.person_outline,
              color: txtColor,
            ),
          ),
        ],
      ),
      body: hasItem ? cartItem() : const CartNoItem(),
    );
  }
  Column cartItem() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 12),
          child: Text(listTmpProd.length.toString() + " ITEMS: "),
        ),
        Expanded(
          child: ListView.builder(
            itemCount: listTmpProd.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                margin: const EdgeInsets.only(bottom: 10),
                height: 150,
                child: Row(
                  children: [
                    Image.asset(listTmpProd[index].image),
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.only(left: 10),
                        height: 150,
                        color: const Color(0xffeceff0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: 30),
                            Text(
                              listTmpProd[index].name,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16
                              ),
                            ),
                            const SizedBox(height: 10,),
                            Container(
                              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                              color: backgroundColor,
                              child: Text(listTmpProd[index].price),
                            ),
                            const SizedBox(height: 12,),
                            listTmpProd[index].isLiked ? Container(
                              width: 75,
                              height: 26,
                              decoration: BoxDecoration(border: Border.all(color: Colors.red)),
                              child: GestureDetector(
                                onTap: () {
                                  listTmpProd[index].isLiked = !listTmpProd[index].isLiked;
                                  setState(() {
                                    
                                  });
                                },
                                child: Row(
                                  children: const [
                                    Text("  SAVE ", style: TextStyle(color: Colors.red),),
                                    Icon(
                                      Icons.favorite,
                                      size: 16,
                                      color: Colors.red
                                    ),
                                  ],
                                ),
                              ),
                            )
                            : Container(
                              width: 75,
                              height: 26,
                              decoration: BoxDecoration(border: Border.all()),
                              child: GestureDetector(
                                onTap: () {
                                  listTmpProd[index].isLiked = !listTmpProd[index].isLiked;
                                  setState(() {
                                    
                                  });
                                },
                                child: Row(
                                  children: const [
                                    Text("  SAVE "),
                                    Icon(
                                      Icons.favorite_border,
                                      size: 16,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (_) => CupertinoAlertDialog(
                              title: const Text("REMOVE"),
                              content: const Text("Are you sure want to remove this item?"),
                              actions: [
                                    FlatButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: const Text(
                                        "Cancel",
                                        style:
                                            TextStyle(color: Colors.blueAccent),
                                      ),
                                    ),
                                    FlatButton(
                                      onPressed: () {
                                        listTmpProd[index].isCart = !listTmpProd[index].isCart;
                                        setState(() {
                                          
                                        });
                                        Navigator.pop(context);
                                      },
                                      child: const Text(
                                        "Yes",
                                        style: TextStyle(color: Colors.red),
                                      ),
                                    ),
                              ],
                        ));
                      },
                      child: Container(
                        padding: const EdgeInsets.only(right: 10),
                        child: const Icon(Icons.more_horiz),
                        color: const Color(0xffeceff0),
                        height: 150,
                      ),
                    )
                  ],
                ),
        
              );
            },
          ),
        ),
        Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Total price: " + total.toString() + " đ",
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
              ),
            ),
          ),
        )
      ],
    );
  }
}

