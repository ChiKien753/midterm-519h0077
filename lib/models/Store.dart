class Store {
  final String name, street, city, country; 
  final int km;

  Store({
    required this.name,
    required this.street,
    required this.city,
    required this.country,
    required this.km,
  });
}

List<Store> stores = [
  Store(
    name: 'PHO-BCS LUY BAN BICH',
    street: 'Luy Ban Bich 801',
    city: 'HCMC',
    country: 'Vietnam',
    km: 1
  ),
  Store(
    name: 'TSN-FOS BINH THOI',
    street: 'Binh Thoi 19',
    city: '700000, Ho Chi Minh',
    country: 'Vietnam',
    km: 3
  ),
  Store(
    name: 'PHO-BCS AEON TAN PHU',
    street: 'Tan Thang 30',
    city: 'Tan Phu, HCM',
    country: 'Vietnam',
    km: 3
  ),
  Store(
    name: 'SPV-FOS CONG HOA',
    street: 'Cong Hoa Str 273',
    city: '700000, Ho Chi Minh',
    country: 'Vietnam',
    km: 3
  ),
];