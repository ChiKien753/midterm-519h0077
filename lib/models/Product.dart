class Product {
  final String name, category, price, image; 
  final int id;
  bool isLiked, isCart;

  Product({
    required this.name,
    required this.category,
    required this.image,
    required this.id,
    required this.price,
    this.isLiked = false,
    this.isCart = false,
  });
}

List<Product> products = [
  Product(
    name: 'ULTRABOOST 22',
    category: 'New Arrival',
    image: 'assets/images/products/new_arrivals/ultraboost22.webp',
    id: 1,
    price: '4.500.000',
  ), 
  Product(
    name: 'TINMAN ELITE',
    category: 'New Arrival',
    image: 'assets/images/products/new_arrivals/tinman_elite.webp',
    id: 2,
    price: '3.500.000',
  ), 
  Product(
    name: 'TYSHAWN',
    category: 'New Arrival',
    image: 'assets/images/products/new_arrivals/tyshawn.webp',
    id: 3,
    price: '3.500.000',
  ), 
  Product(
    name: '4D FWD X PARLEY',
    category: 'New Arrival',
    image: 'assets/images/products/new_arrivals/4d_fwd_x_parley.webp',
    id: 4,
    price: '6.000.000',
  ), 
  Product(
    name: 'Y-3 ULTRABOOST 22',
    category: 'New Arrival',
    image: 'assets/images/products/new_arrivals/y-3_ultraboost.webp',
    id: 5,
    price: '5.500.000',
  ), 
  Product(
    name: 'DAME 8',
    category: 'Basketball Shoes',
    image: 'assets/images/products/basketball/dame8.webp',
    id: 6,
    price: '2.800.000',
  ), 
  Product(
    name: 'TRAE YOUNG 1',
    category: 'Basketball Shoes',
    image: 'assets/images/products/basketball/ty1.webp',
    id: 7,
    price: '3.100.000',
  ), 
  Product(
    name: 'D.O.N ISSUE 2',
    category: 'Basketball Shoes',
    image: 'assets/images/products/basketball/don2.webp',
    id: 8,
    price: '2.500.000',
  ), 
  Product(
    name: 'DAME 7',
    category: 'Basketball Shoes',
    image: 'assets/images/products/basketball/dame7.jpg',
    id: 9,
    price: '2.800.000',
  ), 
  Product(
    name: 'D.O.N ISSUE 3',
    category: 'Basketball Shoes',
    image: 'assets/images/products/basketball/don3.webp',
    id: 10,
    price: '2.800.000',
  ), 
  Product(
    name: 'SHOWER SLIDES',
    category: 'Slides & Sandals',
    image: 'assets/images/products/slides/shower.webp',
    id: 11,
    price: '650.000',
  ), 
  Product(
    name: 'TND SLIDES',
    category: 'Slides & Sandals',
    image: 'assets/images/products/slides/tnd.webp',
    id: 12,
    price: '800.000',
  ), 
  Product(
    name: 'ADILETTE SLIDES',
    category: 'Slides & Sandals',
    image: 'assets/images/products/slides/adilette.webp',
    id: 13,
    price: '1.000.000',
  ), 
  Product(
    name: 'ADILETTE SANDALS',
    category: 'Slides & Sandals',
    image: 'assets/images/products/slides/adilette_sandal.webp',
    id: 14,
    price: '900.000',
  ), 
  Product(
    name: 'NMD R1',
    category: 'Outlet',
    image: 'assets/images/products/outlet/nmd_r1.webp',
    id: 15,
    price: '1.800.000',
  ), 
  Product(
    name: 'ULTRABOOST 20',
    category: 'Outlet',
    image: 'assets/images/products/outlet/ultraboost20.webp',
    id: 16,
    price: '2.500.000',
  ), 
  Product(
    name: 'ADI0 BOSTON 10',
    category: 'Outlet',
    image: 'assets/images/products/outlet/adizero.webp',
    id: 17,
    price: '1.900.000',
  ), 
  Product(
    name: 'ULTRABOOST 21',
    category: 'Outlet',
    image: 'assets/images/products/outlet/ultraboost21.webp',
    id: 18,
    price: '2.500.000',
  ), 
  Product(
    name: 'COURTIC',
    category: 'Outlet',
    image: 'assets/images/products/outlet/courtic.webp',
    id: 19,
    price: '2.125.000',
  ), 
];

List<Product> newArrivals = [
  Product(
    name: 'ULTRABOOST 22',
    category: 'New Arrival',
    image: 'assets/images/products/new_arrivals/ultraboost22.webp',
    id: 1,
    price: '4.500.000',
  ), 
  Product(
    name: 'TINMAN ELITE',
    category: 'New Arrival',
    image: 'assets/images/products/new_arrivals/tinman_elite.webp',
    id: 2,
    price: '3.500.000',
  ), 
  Product(
    name: 'TYSHAWN',
    category: 'New Arrival',
    image: 'assets/images/products/new_arrivals/tyshawn.webp',
    id: 3,
    price: '3.500.000',
  ), 
  Product(
    name: 'Y-3 ULTRABOOST 22',
    category: 'New Arrival',
    image: 'assets/images/products/new_arrivals/y-3_ultraboost.webp',
    id: 4,
    price: '5.500.000',
  ), 
  Product(
    name: '4D FWD X PARLEY',
    category: 'New Arrival',
    image: 'assets/images/products/new_arrivals/4d_fwd_x_parley.webp',
    id: 5,
    price: '6.000.000',
  ), 
];

List<Product> basketball = [
  Product(
    name: 'DAME 8',
    category: 'Basketball Shoes',
    image: 'assets/images/products/basketball/dame8.webp',
    id: 1,
    price: '2.800.000',
  ), 
  Product(
    name: 'TRAE YOUNG 1',
    category: 'Basketball Shoes',
    image: 'assets/images/products/basketball/ty1.webp',
    id: 2,
    price: '3.100.000',
  ), 
  Product(
    name: 'D.O.N ISSUE 2',
    category: 'Basketball Shoes',
    image: 'assets/images/products/basketball/don2.webp',
    id: 3,
    price: '2.500.000',
  ), 
  Product(
    name: 'DAME 7',
    category: 'Basketball Shoes',
    image: 'assets/images/products/basketball/dame7.jpg',
    id: 4,
    price: '2.800.000',
  ), 
  Product(
    name: 'D.O.N ISSUE 3',
    category: 'Basketball Shoes',
    image: 'assets/images/products/basketball/don3.webp',
    id: 5,
    price: '2.800.000',
  ), 
];

List<Product> slides = [
  Product(
    name: 'SHOWER SLIDES',
    category: 'Slides & Sandals',
    image: 'assets/images/products/slides/shower.webp',
    id: 1,
    price: '650.000',
  ), 
  Product(
    name: 'TND SLIDES',
    category: 'Slides & Sandals',
    image: 'assets/images/products/slides/tnd.webp',
    id: 2,
    price: '800.000',
  ), 
  Product(
    name: 'ADILETTE SLIDES',
    category: 'Slides & Sandals',
    image: 'assets/images/products/slides/adilette.webp',
    id: 3,
    price: '1.000.000',
  ), 
  Product(
    name: 'ADILETTE SANDALS',
    category: 'Slides & Sandals',
    image: 'assets/images/products/slides/adilette_sandal.webp',
    id: 4,
    price: '900.000',
  ), 
];

List<Product> outlet = [
  Product(
    name: 'NMD R1',
    category: 'Outlet',
    image: 'assets/images/products/outlet/nmd_r1.webp',
    id: 1,
    price: '1.800.000',
  ), 
  Product(
    name: 'ULTRABOOST 20',
    category: 'Outlet',
    image: 'assets/images/products/outlet/ultraboost20.webp',
    id: 2,
    price: '2.500.000',
  ), 
  Product(
    name: 'ADI0 BOSTON 10',
    category: 'Outlet',
    image: 'assets/images/products/outlet/adizero.webp',
    id: 3,
    price: '1.900.000',
  ), 
  Product(
    name: 'ULTRABOOST 21',
    category: 'Outlet',
    image: 'assets/images/products/outlet/ultraboost21.webp',
    id: 4,
    price: '2.500.000',
  ), 
  Product(
    name: 'COURTIC',
    category: 'Outlet',
    image: 'assets/images/products/outlet/courtic.webp',
    id: 5,
    price: '2.125.000',
  ), 
];