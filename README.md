App có 5 màn hình chính và các màn hình khác như sau:

_Drops: Hiển thị các poster giới thiệu các sản phẩm mới ra mắt của hãng Adidas 
    +Gồm 5 categories có thể chuyển qua chuyển lại giữa các danh mục bằng cách click
    +Mỗi thẻ poster có nút "SHOP NOW" có thể mở được danh sách sản phẩm
    +Góc phải trên app bar có icon kính lúp để mở page search item
    +Góc phải trên app bar có icon hình người để mở page authentication

_Shop: Hiển thị các sản phẩm nổi bật theo thể loại 
    +Có 3 categories Men, Women, Kids
    +Hiển thị các sản phẩm chia theo danh mục theo list view nằm ngang
    +Có thể chọn vào từng sản phẩm để hiển thị thông tin chi tiết
    +Có nút "SEE ALL" để mở toàn bộ sản phẩm đang có trong cửa hàng 
    +Góc phải trên app bar có icon mở page search item
    +Góc phải trên app bar có icon hình người để mở page authentication

_Wishlist: Hiển thị danh sách sản phẩm đã thích
    +Nếu chưa thích bất kỳ sản phẩm nào trong cửa hàng, thì mặc định sẽ là trang trống 
     có nút "START SHOPPING" để mở toàn bộ sản phẩm trong cửa hàng
    +Nếu có sản phẩm đã thích, trang sẽ hiển thị các sản phẩm đã thích với layout gridview
     có icon trái tim màu đỏ
    +Góc phải trên app bar có icon hình người để mở page authentication

_Cart: Hiển thị các sản phẩm trong túi đồ
    +Nếu chưa có sản phẩm trong túi đồ, trang sẽ rỗng và có nút "BROWSE TREND" để mở trang tất cả sản phẩm
    +Hiển thị số lượng sản phẩm đang có trong túi đồ
    +Hiển thị tổng giá trị sản phẩm đang có trong túi đồ
    +Các sản phẩm được add vào túi đồ sẽ được hiển thị với list view hàng dọc
    +Có thể thêm sản phẩm vào danh mục yêu thích
    +Có thể xóa sản phẩm bằng cách nhấn vào nút 3 chấm, hiển thị một dialog để lựa chọn
    +Góc phải trên app bar có icon hình người để mở page authentication

_Radar: Hiển thị danh sách các cửa hàng gần tôi
    +Có 1 poster để hiển thị chương trình mới nhất của app
    +Dach sách các cửa hàng được xếp theo list view nằm ngang
    +Góc phải trên app bar có icon hình người để mở page authentication

_Authentication: Hiển thị 2 phương thức đăng nhập và đăng ký
    +Click vào "LOGIN" để mở screen đăng nhập
    +Click vào "REGISTER" để mở screen đăng ký

_All items: Hiển thị tất cả các sản phẩm đang có trong cửa hàng
    +Có thể bấm like sản phẩm bằng icon trái tim trên góc phải của sản phẩm
    +Góc phải trên app bar có icon kính lúp để mở trang search 
    +Có thể click vào sản phẩm riêng biệt để mở trang chi tiết sản phẩm

_Item detail: Hiển thị các thông tin chi tiết của sản phẩm 
    +Có thể bỏ sản phẩm vào danh mục thích bằng icon trái tim
    +Có thể bỏ sản phẩm vào giỏ đồ bằng nút "ADD TO CART"